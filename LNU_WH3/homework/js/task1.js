// Your code goes here
calculateAmount();

function calculateAmount() {
    const hundred = 100;
    const fixed = 2;

    const checkNumber = +prompt('inputs check number', '');
    const tipPercentage = +prompt('inputs tip percentage', '');

    const tipAmount = checkNumber / hundred * tipPercentage;
    const totalSum = checkNumber + tipAmount;

    const isInvalidValue = !isNumber(checkNumber) || !isNumber(tipPercentage) ||
        (tipPercentage < 0 && tipPercentage >= hundred);

    if (isInvalidValue || totalSum < 0) {
        alert('Invalid input data');
    } else {
        alert(`Check number: ${checkNumber.toFixed(Number.isInteger(checkNumber) ? 0 : fixed)} 
        Tip: ${tipPercentage.toFixed(Number.isInteger(tipPercentage) ? 0 : fixed)}
        Tip amount ${tipAmount.toFixed(Number.isInteger(tipAmount) ? 0 : fixed)}
        Total sum to pay: ${totalSum.toFixed(Number.isInteger(totalSum) ? 0 : fixed)}`);
    }
}

function isNumber(value) {
    return typeof value === "number" && isFinite(value)
}
