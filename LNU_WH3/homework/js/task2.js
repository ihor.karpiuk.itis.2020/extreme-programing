// Your code goes here
const word = prompt('Enter the word:');
const half = 2;

if (word.trim().length === 0) {
    alert('Invalid value');
} else {
    const oddEven = word.length % half === 0;
    const firstChar = word.charAt(word.length / half - 1);
    const secondChar = word.charAt(word.length / half);
    alert(oddEven ? `${firstChar}${secondChar}`: secondChar);
}
