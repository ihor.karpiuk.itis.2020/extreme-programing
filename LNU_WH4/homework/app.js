function reverseNumber(num) {
    return (
        parseFloat(
            num
                .toString()
                .split('')
                .reverse()
                .join('')
        ) * Math.sign(num)
    )
}

function forEach(array, func) {
    for (let i = 0; i < array.length; i++) {
        array[i] = func(array[i])
    }
}

function map(array, func) {
    forEach(array, func)
    return array;
}

function filter(array, func) {
    let initialArr = [...array];
    let filterA = [];
    forEach(array, func)
    for (let i = 0; i < array.length; i++) {
        if (array[i]) {
            filterA.push(initialArr[i]);
        }
    }
    return filterA;
}

function getAdultAppleLovers(data) {
    const favoriteFruit = 'apple';
    let filteredA = filter(data, (el) => el.age > 18 && el.favoriteFruit === favoriteFruit);
    return map(filteredA, (el) => el.name);
}

function getKeys(obj) {
    let res = [];
    for (let i in obj) {
        res.push(i);
    }
    return res;
}

function getValues(obj) {
    const res = [];
    for (let i in obj) {
        res.push(obj[i]);
    }
    return res;
}

function showFormattedDate(dateObj) {
    const dateOptions = {month: 'short'};
    return `It is ${dateObj.getDate()} of ${dateObj.toLocaleString('en', dateOptions)}, ${dateObj.getFullYear()}`;
}

// function runAll(mockData) {
//     console.log('Revers Number', reverseNumber(12345))
//     console.log('Task 2');
//     forEach([2, 5, 8], console.log);

//     console.log('Task 3');
//     console.log(map([2, 5, 8], (el) => el + 3));

//     console.log('Task 4');
//     console.log(filter([2, 5, 1, 3, 8, 6], (el) => el > 3));
//     console.log('Task 4 (Again)');
//     console.log(filter([1, 4, 6, 7, 8, 10], (el) => el % 2 === 0));

//     console.log('Task 5');
//     console.log(getAdultAppleLovers(mockData));

//     console.log('Task 6');
//     console.log(getKeys({keyOne: 1, keyTwo: 2, keyThree: 3}));

//     console.log('Task 7');
//     console.log(getValues({keyOne: 1, keyTwo: 2, keyThree: 3}));

//     console.log('Task 8');
//     console.log(showFormattedDate((new Date('2018-08-27T01:10:00'))));

// }

// const mockData = [
//     {
//         "_id": "5b5e3168c6bf40f2c1235cd6",
//         "index": 0,
//         "age": 39,
//         "eyeColor": "green",
//         "name": "Stein",
//         "favoriteFruit": "apple"
//     },
//     {
//         "_id": "5b5e3168e328c0d72e4f27d8",
//         "index": 1,
//         "age": 38,
//         "eyeColor": "blue",
//         "name": "Cortez",
//         "favoriteFruit": "strawberry"
//     },
//     {
//         "_id": "5b5e3168cc79132b631c666a",
//         "index": 2,
//         "age": 2,
//         "eyeColor": "blue",
//         "name": "Suzette",
//         "favoriteFruit": "apple"
//     },
//     {
//         "_id": "5b5e31682093adcc6cd0dde5",
//         "index": 3,
//         "age": 17,
//         "eyeColor": "green",
//         "name": "Weiss",
//         "favoriteFruit": "banana"
//     }
// ]

// runAll(mockData);
