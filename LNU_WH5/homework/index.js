function isEquals(arg1, arg2) {
    return arg1 === arg2;
}

function numberToString(number) {
    return number.toString();
}

function storeNames(...args) {
    return [...args];
}

function getDivision(first, second) {
    if (isEquals(first, second)) {
        return 1;
    }
    return first > second ? first / second : second / first;

}

function negativeCount(array) {
    return array.filter((el) => el < 0).length;
}

// function runAll() {
//     console.log('Task 1 (True)');
//     console.log(isEquals(3, 3));
//     console.log('Task 1 (False)');
//     console.log(isEquals(3, 4));

//     console.log('Task 2');
//     console.log( typeof numberToString(10));

//     console.log('Task 3');
//     console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'));

//     console.log('Task 4');
//     console.log(getDivision(4, 1));
//     console.log(getDivision(2, 8));

//     console.log('Task 5 (0)');
//     console.log(negativeCount([4, 3, 2, 9]));
//     console.log('Task 5 (1)');
//     console.log(negativeCount([0, -3, 5, 7]));

// }

// runAll();
